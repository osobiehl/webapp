# RECIPE_WEBAPP

A simple API MVC controller using ASP.NET for training at Avántica Technologies


## Usage

Use the ConnectionString specified in appsettings.JSON to connect to the desired database / port.
Only mySQL is currently supported.
controller methods are routed using the /api/[controller]/[action] scheme. This means that
