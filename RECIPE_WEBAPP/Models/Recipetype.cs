﻿using System;
using System.Collections.Generic;

namespace RECIPE_WEBAPP.Models
{
    public partial class Recipetype
    {
        public Recipetype()
        {
            Recipe = new HashSet<Recipe>();
        }

        public int IdRecipeType { get; set; }
        public string Name { get; set; }

        public virtual ICollection<Recipe> Recipe { get; set; }
    }
}
