﻿using System;
using System.Collections.Generic;

namespace RECIPE_WEBAPP.Models

{
    public partial class User
    {
        public User()
        {
            Comment = new HashSet<Comment>();
        }

        public int IdUser { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int? PhoneNumber { get; set; }

        public virtual ICollection<Comment> Comment { get; set; }
    }
}
