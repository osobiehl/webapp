﻿using System;
using System.Collections.Generic;

namespace RECIPE_WEBAPP.Models
{
    public partial class Recipe
    
    {
        public Recipe()
        {
            Comment = new HashSet<Comment>();
        }
        public Recipe(Recipe cpy)
        {
            IdRecipe = cpy.IdRecipe;
            IdRecipeType = cpy.IdRecipeType;
            IdRecipeCategory = cpy.IdRecipeCategory;
            Name = cpy.Name;
            IdOrigin = cpy.IdOrigin;
            IdSpoons = cpy.IdSpoons;
            IdChef = cpy.IdChef;
            Preptime = cpy.Preptime;
            Cooktimeminutes = cpy.Cooktimeminutes;
            Cooktimehours = cpy.Cooktimehours;
            Refrigerationtime = cpy.Refrigerationtime;
            Description = cpy.Description;
            Calories = cpy.Calories;
            Comment = new HashSet<Comment>();


        }
        public int IdRecipe { get; set; }
        public int IdRecipeType { get; set; }
        public int IdRecipeCategory { get; set; }
        public string Name { get; set; }
        public int IdOrigin { get; set; }
        public int IdSpoons { get; set; }
        public int? IdChef { get; set; }
        public int Preptime { get; set; }
        public int Cooktimehours { get; set; }
        public int Refrigerationtime { get; set; }
        public int Cooktimeminutes { get; set; }
        public string Description { get; set; }
        public int? Calories { get; set; }

        public virtual Chef IdChefNavigation { get; set; }
        public virtual Origin IdOriginNavigation { get; set; }
        public virtual Recipecategory IdRecipeCategoryNavigation { get; set; }
        public virtual Recipetype IdRecipeTypeNavigation { get; set; }
        public virtual ICollection<Comment> Comment { get; set; }
    }

    public class RecipeSent : Recipe
    {
        public RecipeSent() : base()
        {
        }
        public RecipeSent(Recipe a) : base(a)
        {
        }
        public string RecipeTypeName { get; set; }
        public string RecipeCategoryName { get; set; }
        public string OriginName { get; set; }
        public string ChefFirstName { get; set; }
        public string ChefLastName { get; set; }
    }
}
