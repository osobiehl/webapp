﻿using System;
using System.Collections.Generic;

namespace RECIPE_WEBAPP.Models
{
    public partial class Chef
    {
        public Chef()
        {
            Recipe = new HashSet<Recipe>();
        }

        public int IdChef { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int? Experience { get; set; }

        public virtual ICollection<Recipe> Recipe { get; set; }
    }
}
