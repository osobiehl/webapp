﻿using System;
using System.Collections.Generic;

namespace RECIPE_WEBAPP.Models

{
    public partial class RecipeIngredientRelation
    {
        public RecipeIngredientRelation(RecipeIngredientRelation cpy)
        {
            if (cpy == null)
                return;
            this.IdRecipeFk = cpy.IdRecipeFk;
            this.Ingredient = cpy.Ingredient;
            this.Amount = cpy.Amount;
            this.Description = cpy.Description;
            this.idRecipeIngredientRelation = cpy.idRecipeIngredientRelation;
        }
        public RecipeIngredientRelation()
        {
        }
        public int IdRecipeFk { get; set; }
        public string Ingredient{ get; set; }
        public int? Amount { get; set; }
        public string Description { get; set; }
        
        public virtual Recipe IdRecipeFkNavigation { get; set; }

        public virtual int idRecipeIngredientRelation { get; set; }
    }
}
