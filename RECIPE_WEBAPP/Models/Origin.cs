﻿using System;
using System.Collections.Generic;

namespace RECIPE_WEBAPP.Models
{
    public partial class Origin
    {
        public Origin()
        {
            Recipe = new HashSet<Recipe>();
        }

        public int IdOrigin { get; set; }
        public string Name { get; set; }

        public virtual ICollection<Recipe> Recipe { get; set; }
    }
}
