﻿using System;
using System.Collections.Generic;

namespace RECIPE_WEBAPP.Models

{
    public partial class RecipeView
    {
        public int RecipeId { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public string Category { get; set; }
        
        public int Spoons { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public int PreparationTime { get; set; }
        public int CookingTimeHours { get; set; }
        public int CookingTimeMinutes { get; set; }
        public string Description { get; set; }
    }
}
