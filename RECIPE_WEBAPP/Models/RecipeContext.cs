﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using System.Configuration;
namespace RECIPE_WEBAPP.Models
{
    public partial class recipeContext : DbContext
    {
        public recipeContext()
        {
        }

        public recipeContext(DbContextOptions<recipeContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Chef> Chef { get; set; }
        public virtual DbSet<Comment> Comment { get; set; }
        public virtual DbSet<Origin> Origin { get; set; }
        public virtual DbSet<Recipe> Recipe { get; set; }
        public virtual DbSet<RecipeView> RecipeView { get; set; }
        public virtual DbSet<Recipecategory> Recipecategory { get; set; }
        public virtual DbSet<RecipeIngredientRelation> RecipeIngredientRelation { get; set; }
        public virtual DbSet<Recipetype> Recipetype { get; set; }
        public virtual DbSet<User> User { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
               //optionsBuilder.UseMySQL(ConfigurationManager.ConnectionStrings["admin"].ConnectionString);
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Chef>(entity =>
            {
                entity.HasKey(e => e.IdChef)
                    .HasName("PRIMARY");

                entity.ToTable("chef");

                entity.HasIndex(e => e.IdChef)
                    .HasName("idChef_UNIQUE")
                    .IsUnique();

                entity.Property(e => e.IdChef).HasColumnName("idChef");

                entity.Property(e => e.Experience).HasColumnName("experience");

                entity.Property(e => e.FirstName)
                    .IsRequired()
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.LastName)
                    .IsRequired()
                    .HasMaxLength(45)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Comment>(entity =>
            {
                entity.HasKey(e => e.IdComment)
                    .HasName("PRIMARY");

                entity.ToTable("comment");

                entity.HasIndex(e => e.IdComment)
                    .HasName("idComment_UNIQUE")
                    .IsUnique();

                entity.HasIndex(e => e.IdRecipe)
                    .HasName("idRecipe_idx");

                entity.HasIndex(e => e.IdUser)
                    .HasName("idUser_idx");

                entity.Property(e => e.IdComment).HasColumnName("idComment");

                entity.Property(e => e.Description)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.IdRecipe).HasColumnName("idRecipe");

                entity.Property(e => e.IdUser).HasColumnName("idUser");

                entity.HasOne(d => d.IdRecipeNavigation)
                    .WithMany(p => p.Comment)
                    .HasForeignKey(d => d.IdRecipe)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("idRecipe");

                entity.HasOne(d => d.IdUserNavigation)
                    .WithMany(p => p.Comment)
                    .HasForeignKey(d => d.IdUser)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("idUser");
            });

            modelBuilder.Entity<Origin>(entity =>
            {
                entity.HasKey(e => e.IdOrigin)
                    .HasName("PRIMARY");

                entity.ToTable("origin");

                entity.HasIndex(e => e.IdOrigin)
                    .HasName("idOrigin_UNIQUE")
                    .IsUnique();

                entity.Property(e => e.IdOrigin).HasColumnName("idOrigin");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(45)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Recipe>(entity =>
            {
                entity.HasKey(e => e.IdRecipe)
                    .HasName("PRIMARY");

                entity.ToTable("recipe");

                entity.HasIndex(e => e.IdChef)
                    .HasName("idChef_idx");

                entity.HasIndex(e => e.IdOrigin)
                    .HasName("idOrigin_idx");

                entity.HasIndex(e => e.IdRecipe)
                    .HasName("idRecipes_UNIQUE")
                    .IsUnique();

                entity.HasIndex(e => e.IdRecipeCategory)
                    .HasName("idRecipeCategory_idx");

                entity.HasIndex(e => e.IdRecipeType)
                    .HasName("idRecipeType_idx");

                entity.HasIndex(e => e.Name)
                    .HasName("name_UNIQUE")
                    .IsUnique();

                entity.Property(e => e.IdRecipe).HasColumnName("idRecipe");

                entity.Property(e => e.Description)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.IdChef).HasColumnName("idChef");

                entity.Property(e => e.IdOrigin).HasColumnName("idOrigin");

                entity.Property(e => e.IdRecipeCategory).HasColumnName("idRecipeCategory");

                entity.Property(e => e.IdRecipeType).HasColumnName("idRecipeType");

                entity.Property(e => e.IdSpoons).HasColumnName("idSpoons");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.HasOne(d => d.IdChefNavigation)
                    .WithMany(p => p.Recipe)
                    .HasForeignKey(d => d.IdChef)
                    .HasConstraintName("idChef");

                entity.HasOne(d => d.IdOriginNavigation)
                    .WithMany(p => p.Recipe)
                    .HasForeignKey(d => d.IdOrigin)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("idOrigin");

                entity.HasOne(d => d.IdRecipeCategoryNavigation)
                    .WithMany(p => p.Recipe)
                    .HasForeignKey(d => d.IdRecipeCategory)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("idRecipeCategory");

                entity.HasOne(d => d.IdRecipeTypeNavigation)
                    .WithMany(p => p.Recipe)
                    .HasForeignKey(d => d.IdRecipeType)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("idRecipeType");
            });

            modelBuilder.Entity<RecipeView>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("recipe_view");

                entity.Property(e => e.Category)
                    .IsRequired()
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.CookingTimeHours).HasColumnName("Cooking Time (Hours)");

                entity.Property(e => e.CookingTimeMinutes).HasColumnName("Cooking time (minutes)");

                entity.Property(e => e.Description)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.Firstname)
                    .IsRequired()
                    .HasColumnName("firstname")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.Lastname)
                    .IsRequired()
                    .HasColumnName("lastname")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.PreparationTime).HasColumnName("Preparation Time");

                entity.Property(e => e.RecipeId).HasColumnName("Recipe id");

                entity.Property(e => e.Type)
                    .IsRequired()
                    .HasMaxLength(45)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Recipecategory>(entity =>
            {
                entity.HasKey(e => e.IdRecipeCategory)
                    .HasName("PRIMARY");

                entity.ToTable("recipecategory");

                entity.HasIndex(e => e.IdRecipeCategory)
                    .HasName("idRecipeCategory_UNIQUE")
                    .IsUnique();

                entity.Property(e => e.IdRecipeCategory).HasColumnName("idRecipeCategory");

                entity.Property(e => e.IdRecipeType).HasColumnName("idRecipeType");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(45)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<RecipeIngredientRelation>(entity =>
            {
                entity.HasKey(e => e.idRecipeIngredientRelation).HasName("PRIMARY");

                entity.ToTable("recipeingredientrelation");

                entity.HasIndex(e => e.IdRecipeFk)
                    .HasName("idRecipe_idx");

                entity.Property(e => e.Amount).HasColumnName("amount");

                entity.Property(e => e.Ingredient).HasColumnName("Ingredient");

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasColumnName("description")
                    .HasMaxLength(250)
                    .IsUnicode(false);
                

                entity.Property(e => e.IdRecipeFk).HasColumnName("idRecipe_fk");
                

                entity.HasOne(d => d.IdRecipeFkNavigation)
                    .WithMany()
                    .HasForeignKey(d => d.IdRecipeFk)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("idRecipe_fk");
            });

            modelBuilder.Entity<Recipetype>(entity =>
            {
                entity.HasKey(e => e.IdRecipeType)
                    .HasName("PRIMARY");

                entity.ToTable("recipetype");

                entity.HasIndex(e => e.IdRecipeType)
                    .HasName("idRecipeType_UNIQUE")
                    .IsUnique();

                entity.HasIndex(e => e.Name)
                    .HasName("name_UNIQUE")
                    .IsUnique();

                entity.Property(e => e.IdRecipeType).HasColumnName("idRecipeType");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(45)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<User>(entity =>
            {
                entity.HasKey(e => e.IdUser)
                    .HasName("PRIMARY");

                entity.ToTable("user");

                entity.HasIndex(e => e.IdUser)
                    .HasName("idUser_UNIQUE")
                    .IsUnique();

                entity.Property(e => e.IdUser).HasColumnName("idUser");

                entity.Property(e => e.FirstName)
                    .IsRequired()
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.LastName)
                    .IsRequired()
                    .HasMaxLength(45)
                    .IsUnicode(false);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
