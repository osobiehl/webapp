﻿using System;
using System.Collections.Generic;

namespace RECIPE_WEBAPP.Models

{
    public partial class Comment
    {
        public Comment(Comment cpy)
        {
            this.IdComment = cpy.IdComment;
            this.IdUser = cpy.IdUser;
            this.IdRecipe = cpy.IdRecipe;
            this.Description = cpy.Description;
            this.Created = cpy.Created;
        }

        public Comment()
        {
        }

        public int IdComment { get; set; }
        public int IdUser { get; set; }
        public int IdRecipe { get; set; }
        public string Description { get; set; }
        public DateTime Created { get; set; }

        public virtual Recipe IdRecipeNavigation { get; set; }
        public virtual User IdUserNavigation { get; set; }
    }
    public class CommentSent : Comment
    {
        public CommentSent(Comment c) : base(c)
        {
            
        }
        public CommentSent() : base()
        {
            
        }
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}
