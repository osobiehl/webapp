﻿using System;
using System.Collections.Generic;

namespace RECIPE_WEBAPP.Models
{
    public partial class Recipecategory
    {
        public Recipecategory()
        {
            Recipe = new HashSet<Recipe>();
        }

        public int IdRecipeCategory { get; set; }
        public string Name { get; set; }
        public int IdRecipeType { get; set; }

        public virtual ICollection<Recipe> Recipe { get; set; }
    }
}
