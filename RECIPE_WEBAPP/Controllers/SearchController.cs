﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using RECIPE_WEBAPP.Models;


namespace RECIPE_WEBAPP.Controllers
{    
    [Route("api/search/[action]/")]
    [ApiController]
    public class SearchController : ControllerBase
    {
        private readonly recipeContext _context;

        public SearchController(recipeContext context)
        {
            _context = context;
        }

        // GET: api/search/ NOT DEFINED


        // GET: api/search/RecipeName=Gallo
        // returns a list of recipes with the current substring
        [HttpGet("RecipeName={Rname}")]
        public async Task<ActionResult<IEnumerable<RecipeSent>>> GetRecipe(string Rname)
        {
            //TODO SANITIZE STRING INPUT

            var recipe = await _context.Recipe.Where(v => v.Name.Contains(Rname)).ToListAsync();
            if (recipe == null)
                return NotFound();
            List<RecipeSent> res = new List<RecipeSent>();
            foreach (var temp in recipe)
            {
                res.Add(ConvertToRecipeSent(temp) );
            }
            return res;
        }
        
        [HttpGet("RecipeName={Rname}")]
        public async Task<ActionResult<IEnumerable<RecipeView>>> GetRecipeView(string Rname)
        {
            //TODO SANITIZE STRING INPUT

            var recipe = await _context.RecipeView.Where(v => v.Name.Contains(Rname)).ToListAsync();
            return recipe;
        }

        public RecipeSent ConvertToRecipeSent(Recipe recipe)
        {
            var res = new RecipeSent(recipe);
            var relatedChef = _context.Chef.Find(recipe.IdChef);
            res.ChefFirstName = relatedChef.FirstName;
            res.ChefLastName = relatedChef.LastName;
            res.OriginName = _context.Origin.Find(recipe.IdOrigin).Name;
            res.RecipeCategoryName = _context.Recipecategory.Find(recipe.IdRecipeCategory).Name;
            res.RecipeTypeName = _context.Recipetype.Find(recipe.IdRecipeType).Name;
            return res;
        }
        
    }
}