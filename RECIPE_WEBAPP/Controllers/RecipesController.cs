﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing.Constraints;
using Microsoft.EntityFrameworkCore;
using RECIPE_WEBAPP.Models;
using SQLitePCL;

namespace RECIPE_WEBAPP.Controllers
{
    [Route("api/Recipes/[action]/")]
    [ApiController]
    public class RecipesController : ControllerBase
    {
        private readonly recipeContext _context;
        public RecipesController(recipeContext context)
        {
            _context = context;
        }

        // GET: api/Recipes
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Recipe>>> GetRecipe()
        {
            Response.Headers.Add("test", "please work");
            return await _context.Recipe.OrderBy(r => r.Name).
                ToListAsync();
            
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<RecipeSent>>> GetRecipeByType()
        {
            var res = await _context.Recipe.OrderBy(r => r.IdRecipeType).ThenBy(r => r.IdSpoons).ToListAsync();
            var resList = new List<RecipeSent>();
            foreach(var i in res)
            {
                resList.Add(ConvertToRecipeSent(i));
            }
            return resList;
        }
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Recipe>>> GetRecipeByCategory()
        {
            var res =  await _context.Recipe.OrderBy(r => r.IdRecipeCategory)
                .ThenBy(r => r.IdRecipeType)
                .ThenByDescending(r => r.IdSpoons)
                .ToListAsync();
            
            var resList = new List<RecipeSent>();
            foreach(var i in res)
            {
                resList.Add(ConvertToRecipeSent(i));
            }
            return resList;
        }
        // the hackiest method known to man IMPROVE IF NEEDED-
        [HttpGet]
        public IEnumerable<RecipeSent> GetMajorRankByCategory()
        {
            List<RecipeSent> res = new List<RecipeSent>();
            int m = _context.Recipe.Max(R => R.IdRecipeCategory);
            for (int i = 0; i < m; i++)
            //selects all recipies based on type, finds the item with the greatest spoons
            {
                var b = _context.Recipe.Where(R => R.IdRecipeCategory.CompareTo(i) == 0);
                if (!b.Any())
                    continue;
                var thing = b.Max(R => R.IdSpoons);
                var l = (b.Where(R => R.IdSpoons == thing).ToList());
                //returns a generic enumerable ofsize 1, we pass it to a list
                foreach (var v in l)
                {
                    res.Add(ConvertToRecipeSent(v));
                }
            }
            return res;
        }

        public RecipeSent ConvertToRecipeSent(Recipe recipe)
        {
            var res = new RecipeSent(recipe);
            var relatedChef = _context.Chef.Find(recipe.IdChef);
            res.ChefFirstName = relatedChef.FirstName;
            res.ChefLastName = relatedChef.LastName;
            res.OriginName = _context.Origin.Find(recipe.IdOrigin).Name;
            res.RecipeCategoryName = _context.Recipecategory.Find(recipe.IdRecipeCategory).Name;
            res.RecipeTypeName = _context.Recipetype.Find(recipe.IdRecipeType).Name;
            return res;
        }
        [HttpGet("{id}")]
        public async Task<ActionResult<RecipeSent>> GetRecipe(int id)
        {
            var baseResult = await _context.Recipe.FindAsync(id);
            if (baseResult == null)
                return NotFound();
            return ConvertToRecipeSent(baseResult);
        }

        [HttpGet("{id}")]
        // gets all ingredients associated to a recipe


        public async Task<ActionResult<IEnumerable<RecipeIngredientRelation>>> GetIngredients(int id)
        {
            var res = await _context.RecipeIngredientRelation.Where(r => r.IdRecipeFk == id).ToListAsync();
            if (res == null)
                return NotFound();
            return res;
        }
        // api/recipes/getRecipeIngredientRleation
        [HttpGet]
        public async Task<ActionResult<IEnumerable<RecipeIngredientRelation>>> GetRecipeIngredientRelation()
        {
            return await _context.RecipeIngredientRelation.ToListAsync();
        }

        [HttpGet("{id}")]
        //gets RIR class associated to the id
        public async Task<ActionResult<RecipeIngredientRelation>> GetRecipeIngredientRelation(int id)
        {
            var res = await _context.RecipeIngredientRelation.FindAsync(id);
            
            if (res == null)
                return NotFound();
            return res;
        }


        //method to post new RecipeIngredientRelation
        [HttpPost]
        public async Task<ActionResult<Recipe>> PostRecipeIngredientRelation(RecipeIngredientRelation RIR)
        {
            try
            {
                _context.RecipeIngredientRelation.Add(RIR);

                await _context.SaveChangesAsync();
            }
            catch (Exception e)
            {
                return BadRequest();
            }
            // TEST TO SEE IF CORS WILL FINALLY WORK
            HttpContext.Request.Headers.Add("Access-Control-Allow-Headers", "content-type");
            
            return CreatedAtAction("PostRecipeIngredientRelation", new { id_recipe = RIR.IdRecipeFk}, RIR);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> PutRecipeIngredientRelation(int id, RecipeIngredientRelation RIR)
        {
            if (id != RIR.idRecipeIngredientRelation)
                return BadRequest();
            _context.Entry(RIR).State = EntityState.Modified;
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!RecipeIngredientRelationExists(id))
                    return NotFound();
                else
                    throw;
            }
            return NoContent();
        }



        // PUT: Recipe/PutRecipe/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutRecipe(int id, RecipeSent recipe)
        {
            if (id != recipe.IdRecipe)
            {
                return BadRequest();
            }
            VerifyChef(recipe); 
            VerifyOrigin(recipe);
            _context.Entry(recipe).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!RecipeExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
            return NoContent();
        }

        // POST: recipes/PostRecipe/
        [HttpPost]
        public async Task<ActionResult<Recipe>> PostRecipe(RecipeSent recipe)
        {
            VerifyChef(recipe);
            VerifyOrigin(recipe);
            try
            {
                await _context.Recipe.AddAsync((Recipe) recipe);

                await _context.SaveChangesAsync();
            }
            catch (Exception a)
            {
                return BadRequest();
            }

            return CreatedAtAction("GetRecipe", new { id = recipe.IdRecipe }, (recipe));
            
        }
        

        // DELETE: Recipes/DeleteRecipe/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Recipe>> DeleteRecipe(int id)
        {
            DeleteComments(id);
            var recipe = await _context.Recipe.FindAsync(id);
            if (recipe == null)
            {
                return NotFound();
            }
            _context.Recipe.Remove(recipe);
            await _context.SaveChangesAsync();
            return recipe;
        }

        private async void VerifyChef(RecipeSent recipe)
        {
            var verifyChef = _context.Chef.FirstOrDefault(c =>
                recipe.ChefFirstName == c.FirstName && recipe.ChefLastName == c.LastName);
            if (verifyChef == null)
            {
                var newChef = new Chef
                {
                    FirstName = recipe.ChefFirstName, LastName = recipe.ChefLastName, Experience = 0
                };
                await _context.Chef.AddAsync(newChef);
                await _context.SaveChangesAsync();
                verifyChef = newChef;
            }
            recipe.IdChef = verifyChef.IdChef;
        }

        private async void VerifyOrigin(RecipeSent recipe)
        {
            var verifyOrigin = _context.Origin.FirstOrDefault(o => o.Name == recipe.OriginName);
            if (verifyOrigin == null)
            {
                var newOrigin = new Origin
                {
                    Name =  recipe.OriginName
                };
                verifyOrigin = newOrigin;
                await _context.Origin.AddAsync(verifyOrigin);
                await _context.SaveChangesAsync();
            }

            recipe.IdOrigin = verifyOrigin.IdOrigin;
        }

        private bool RecipeExists(int id)
        {
            return _context.Recipe.Any(e => e.IdRecipe == id);
        }

        private bool RecipeIngredientRelationExists(int id)
        {
            return _context.RecipeIngredientRelation.Any(e => e.idRecipeIngredientRelation == id);
        }

        private void DeleteComments (int id)
        {
            var res = _context.Comment.Where(com => com.IdRecipe == id).ToList();
            foreach (var i in res)
            {
                _context.Comment.Remove(i);
            }
            _context.SaveChanges();
        }
        
        [HttpDelete("{id}")]
        public async Task<ActionResult<RecipeIngredientRelation>> DeleteRecipeIngredientRelation(int id)
        {
            var res = await _context.RecipeIngredientRelation.FindAsync(id);
            _context.RecipeIngredientRelation.Remove(res);
            await _context.SaveChangesAsync();
            return res;
        }
        
        [HttpGet]
        public async Task<ActionResult<IEnumerable<RecipeView>>> GetRecipeView()
        {
            return await _context.RecipeView.OrderBy(r => r.Name).
                ToListAsync();
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<RecipeView>>> GetRecipeViewByType()
        {

            return await _context.RecipeView.OrderBy(r => r.Type).ThenBy(r => r.Spoons).ToListAsync();
        }
        [HttpGet]
        public async Task<ActionResult<IEnumerable<RecipeView>>> GetRecipeViewByCategory()
        {
            return await _context.RecipeView.OrderBy(r => r.Category)
                .ThenBy(r => r.Type)
                .ThenByDescending(r => r.Spoons)
                .ToListAsync();
        }
        
        [HttpGet("{id}")]
        public   RecipeView GetRecipeView(int id)
        {
            var res = _context.RecipeView.First(rv => rv.RecipeId == id);
            return res;
        }
        //returns all recipe categories associated to a type
        
        [HttpGet("{typeId}")]
        public async Task<ActionResult<IEnumerable<Recipecategory>>> GetRecipeCategoryFromType(int typeId)
        {
            return await _context.Recipecategory.Where(rc=> rc.IdRecipeType == typeId).ToListAsync();
        }
        
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Recipetype>>> GetRecipeType()
        {
            return await _context.Recipetype.ToListAsync();
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<Recipecategory>>> GetRecipeCategory()
        {
            return await _context.Recipecategory.ToListAsync();
        }
    }
}
