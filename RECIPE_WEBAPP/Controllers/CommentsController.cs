﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace RECIPE_WEBAPP.Models
{
    [Route("api/[controller]/[action]/")]
    [ApiController]
    public class CommentsController : ControllerBase
    {
        private readonly recipeContext _context;

        public CommentsController(recipeContext context)
        {
            _context = context;
        }

        // GET: api/Comments
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Comment>>> GetComment()
        {
            return await _context.Comment.ToListAsync();
        }

        // GET: api/Comments/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Comment>> GetCommentById(int id)
        {
            var comment = await _context.Comment.FindAsync(id);

            if (comment == null)
            {
                return NotFound();
            }

            return comment;
        }

        [HttpGet("{Recipe_id}")]
        public async Task<ActionResult<IEnumerable<CommentSent>>> GetCommentRecipe(int Recipe_id)
        {
            var comment = await _context.Comment.Where(com => com.IdRecipe == Recipe_id).OrderBy(com => com.Created).ToListAsync();
            var res = new List<CommentSent>();
            foreach (var c in comment)
            {
                var temp = new CommentSent(c);
                var userInfo = _context.User.First(u => u.IdUser == c.IdUser);
                temp.FirstName = userInfo.FirstName;
                temp.LastName = userInfo.LastName;
                res.Add(temp);
            }

            return res;
        }

        // PUT: api/Comments/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutComment(int id, Comment comment)
        {
            if (id != comment.IdComment)
            {
                return BadRequest();
            }

            _context.Entry(comment).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CommentExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Comments
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Comment>> PostComment(CommentSent comment)
        {
            VerifyComment(comment);
            _context.Comment.Add(comment);
            await _context.SaveChangesAsync();
            return CreatedAtAction("GetComment", new { id = comment.IdComment }, comment);
        }

        private async void VerifyComment(CommentSent comment)
        {
            
            var verifyUser = _context.User.FirstOrDefault(u =>  u.FirstName == comment.FirstName && u.LastName == comment.LastName);
            
            if (verifyUser == null)
            {
                var newUser = new User()
                {
                    FirstName =  comment.FirstName, LastName = comment.LastName
                };
                verifyUser = newUser;
                await _context.User.AddAsync(verifyUser);
                await _context.SaveChangesAsync();
            }

            comment.IdUser= verifyUser.IdUser;
        }

        // DELETE: api/Comments/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Comment>> DeleteComment(int id)
        {
            var comment = await _context.Comment.FindAsync(id);
            if (comment == null)
            {
                return NotFound();
            }

            _context.Comment.Remove(comment);
            await _context.SaveChangesAsync();

            return comment;
        }

        private bool CommentExists(int id)
        {
            return _context.Comment.Any(e => e.IdComment == id);
        }
    }
}
