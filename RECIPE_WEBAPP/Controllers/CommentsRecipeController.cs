﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using RECIPE_WEBAPP.Models;

namespace RECIPE_WEBAPP.Controllers
{
    [Route("api/[controller]/[action]/")]
    [ApiController]
    public class CommentsRecipeController : ControllerBase
    {
        private readonly recipeContext _context;

        public CommentsRecipeController(recipeContext context)
        {
            _context = context;
        }

        // GET: api/CommentsRecipe
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Comment>>> GetComment()
        {
            return await _context.Comment.ToListAsync();
        }

        // GET: api/CommentsRecipe/GetCommentFromRecipe
        [HttpGet("{id}")]
        public async Task<ActionResult<IEnumerable<Comment>>> GetCommentFromRecipe(int id)
        {
            var comment = await _context.Comment.Where(com => com.IdRecipe == id).ToListAsync();
            if (comment == null)
                return NotFound();
            return comment;
        }


        // PUT: api/CommentsRecipe/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        /*
        [HttpPut("{id}")]
        public async Task<IActionResult> PutComment(int id, Comment comment)
        {
            if (id != comment.IdComment)
            {
                return BadRequest();
            }

            _context.Entry(comment).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CommentExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }
       */

        // POST: api/CommentsRecipe
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Comment>> PostComment(Comment comment)
        {
            _context.Comment.Add(comment);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetComment", new { id = comment.IdComment }, comment);
        }

        // DELETE: api/CommentsRecipe/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Comment>> DeleteComment(int id)
        {
            var comment = await _context.Comment.FindAsync(id);
            if (comment == null)
            {
                return NotFound();
            }

            _context.Comment.Remove(comment);
            await _context.SaveChangesAsync();

            return comment;
        }

        private bool CommentExists(int id)
        {
            return _context.Comment.Any(e => e.IdComment == id);
        }
    }
}
